package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.*;
//import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
//import java.io.StringReader;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private NamedNodeMap namespace;
	//private String namespace;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public List<Flower> parse() throws IOException, ParserConfigurationException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		//InputSource is = new InputSource(new StringReader(xmlFileName));
		DocumentBuilder db = factory.newDocumentBuilder();
		//Document doc = db.parse(is);
		Document doc = db.parse(new File(xmlFileName));
		doc.normalize();
		//namespace = doc.getDocumentElement().getNamespaceURI();
		namespace = doc.getDocumentElement().getAttributes();
		//NodeList flowers = doc.getDocumentElement().getChildNodes();
		NodeList flowers = doc.getElementsByTagName("flower");
		List<Flower> list = new LinkedList<>();
		Element current;
		Element temp;
		Flower flower;
		//Flower.VisualParameters param;
		//Flower.GrowingTips tips;

		for (int i = 0; i < flowers.getLength(); i++){
			//flowers.item(i);
			flower = new Flower();
			current = (Element) flowers.item(i);
			flower.setName(current.getElementsByTagName("name").item(0).getTextContent());
			flower.setSoil(current.getElementsByTagName("soil").item(0).getTextContent());
			flower.setOrigin(current.getElementsByTagName("origin").item(0).getTextContent());
			temp = (Element) current.getElementsByTagName("visualParameters").item(0);
			flower.setVisualParameters(new Flower.VisualParameters());
			flower.getVisualParameters().setStemColour(temp.getElementsByTagName("stemColour")
					.item(0).getTextContent());
			flower.getVisualParameters().setLeafColour(temp.getElementsByTagName("leafColour")
					.item(0).getTextContent());
			flower.getVisualParameters().setAveLenFlower(new Flower.VisualParameters.AveLenFlower());
			flower.getVisualParameters().getAveLenFlower().setValue(Integer.parseInt(temp
					.getElementsByTagName("aveLenFlower")
					.item(0).getTextContent()));
			flower.getVisualParameters().getAveLenFlower().setMeasure(((Element) temp
					.getElementsByTagName("aveLenFlower").item(0)).getAttribute("measure"));
			temp = (Element) current.getElementsByTagName("growingTips").item(0);
			flower.setGrowingTips(new Flower.GrowingTips());
			flower.getGrowingTips().setTempreture(new Flower.GrowingTips.Tempreture());
			flower.getGrowingTips().getTempreture().setValue(Integer.parseInt(temp.getElementsByTagName("tempreture")
					.item(0).getTextContent()));
			flower.getGrowingTips().getTempreture().setMeasure(((Element) temp.getElementsByTagName("tempreture")
					.item(0)).getAttribute("measure"));
			flower.getGrowingTips().setLighting(Flower.GrowingTips.Lighting.value(((Element) temp
					.getElementsByTagName("lighting").item(0)).getAttribute("lightRequiring")));
			flower.getGrowingTips().setWatering(new Flower.GrowingTips.Watering());
			flower.getGrowingTips().getWatering().setValue(Integer.parseInt(temp.getElementsByTagName("watering")
					.item(0).getTextContent()));
			flower.getGrowingTips().getWatering().setMeasure(((Element) temp
					.getElementsByTagName("watering").item(0)).getAttribute("measure"));
			flower.setMultiplying(current.getElementsByTagName("multiplying").item(0).getTextContent());
			list.add(flower);
		}
		return list;
	}

	public void FileOutputXML(String outputXmlFile, Collection<Flower> items) throws ParserConfigurationException,
			TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = factory.newDocumentBuilder();
		Document doc = db.newDocument();
		Element root = doc.createElement("flowers");
		//root.setAttribute("xmlns", namespace);
		if (namespace != null) {
			for (int i = 0; i < namespace.getLength(); i++) {
				root.setAttribute(((Attr) namespace.item(i)).getName(), ((Attr) namespace.item(i)).getValue());
			}
			namespace = null;
		}
		doc.appendChild(root);
		Element current;
		Element element;
		Element innerElement;
		//Attr temp;
		for (Flower i : items){
			current = doc.createElement("flower");
			root.appendChild(current);
			element = doc.createElement("name");
			element.appendChild(doc.createTextNode(i.getName()));
			current.appendChild(element);
			element = doc.createElement("soil");
			element.appendChild(doc.createTextNode(i.getSoil()));
			current.appendChild(element);
			element = doc.createElement("origin");
			element.appendChild(doc.createTextNode(i.getOrigin()));
			current.appendChild(element);

			element = doc.createElement("visualParameters");
			innerElement = doc.createElement("stemColour");
			innerElement.appendChild(doc.createTextNode(i.getVisualParameters().getStemColour()));
			element.appendChild(innerElement);
			innerElement = doc.createElement("leafColour");
			innerElement.appendChild(doc.createTextNode(i.getVisualParameters().getLeafColour()));
			element.appendChild(innerElement);
			innerElement = doc.createElement("aveLenFlower");
			innerElement.setAttribute("measure", i.getVisualParameters().getAveLenFlower().getMeasure());
			innerElement.appendChild(doc.createTextNode(i.getVisualParameters().getAveLenFlower().getValue()
					.toString()));
			element.appendChild(innerElement);

			current.appendChild(element);

			element = doc.createElement("growingTips");
			innerElement = doc.createElement("tempreture");
			innerElement.setAttribute("measure", i.getGrowingTips().getTempreture().getMeasure());
			innerElement.appendChild(doc.createTextNode(i.getGrowingTips().getTempreture().getValue().toString()));
			element.appendChild(innerElement);
			innerElement = doc.createElement("lighting");
			innerElement.setAttribute("lightRequiring", i.getGrowingTips().getLighting().get());
			element.appendChild(innerElement);
			innerElement = doc.createElement("watering");
			innerElement.setAttribute("measure", i.getGrowingTips().getWatering().getMeasure());
			innerElement.appendChild(doc.createTextNode(i.getGrowingTips().getWatering().getValue().toString()));
			element.appendChild(innerElement);

			current.appendChild(element);

			element = doc.createElement("multiplying");
			element.appendChild(doc.createTextNode(i.getMultiplying()));
			current.appendChild(element);
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource domSource = new DOMSource(doc);
		StreamResult streamResult = new StreamResult(new File(outputXmlFile));
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,"yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		transformer.transform(domSource, streamResult);
	}
}
