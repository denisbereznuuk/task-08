package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.*;
import java.util.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private Iterator<Attribute> attributes;
	private Iterator<Namespace> namespaces;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public List <Flower> parse(){
		List <Flower> flowers = new LinkedList<>();
		Flower flower = null;
		XMLInputFactory xmlFactory = XMLInputFactory.newInstance();
		try {
			XMLEventReader reader = xmlFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (reader.hasNext()){
				XMLEvent event = reader.nextEvent();
				if (event.isStartElement()){
					StartElement startElement = event.asStartElement();
					String name = startElement.getName().getLocalPart();
					if (name.equals("flowers")){
						attributes = startElement.getAttributes();
						namespaces = startElement.getNamespaces();
					}
					if (name.equals("flower")){
						flower = new Flower();
					} else if(name.equals("name")) {
						event = reader.nextEvent();
						flower.setName(event.asCharacters().getData());
					} else if (name.equals("soil")) {
						event = reader.nextEvent();
						flower.setSoil(event.asCharacters().getData());
					} else if (name.equals("origin")) {
						event = reader.nextEvent();
						flower.setOrigin(event.asCharacters().getData());
					} else if (name.equals("visualParameters")) {
						flower.setVisualParameters(new Flower.VisualParameters());
					} else if (name.equals("stemColour")){
						event = reader.nextEvent();
						flower.getVisualParameters().setStemColour(event.asCharacters().getData());
					} else if (name.equals("leafColour")) {
						event = reader.nextEvent();
						flower.getVisualParameters().setLeafColour(event.asCharacters().getData());
					} else if (name.equals("aveLenFlower")) {
						event = reader.nextEvent();
						flower.getVisualParameters().setAveLenFlower(new Flower.VisualParameters.AveLenFlower());
						flower.getVisualParameters().getAveLenFlower().setValue(Integer.parseInt(event.asCharacters().getData()));
						Attribute attribute = startElement.getAttributeByName(new QName("measure"));
						flower.getVisualParameters().getAveLenFlower().setMeasure(attribute.getValue());
					} else if (name.equals("growingTips")) {
						flower.setGrowingTips(new Flower.GrowingTips());//new String(ch, start, length));
					} else if (name.equals("tempreture")) {
						event = reader.nextEvent();
						flower.getGrowingTips().setTempreture(new Flower.GrowingTips.Tempreture());
						flower.getGrowingTips().getTempreture().setValue(Integer.parseInt(event.asCharacters().getData()));
						Attribute attribute = startElement.getAttributeByName(new QName("measure"));
						flower.getGrowingTips().getTempreture().setMeasure(attribute.getValue());
					} else if (name.equals("watering")) {
						event = reader.nextEvent();
						flower.getGrowingTips().setWatering(new Flower.GrowingTips.Watering());
						flower.getGrowingTips().getWatering().setValue(Integer.parseInt(event.asCharacters().getData()));
						Attribute attribute = startElement.getAttributeByName(new QName("measure"));
						flower.getGrowingTips().getWatering().setMeasure(attribute.getValue());
					} else if (name.equals("lighting")) {
						Attribute attribute = startElement.getAttributeByName(new QName("lightRequiring"));
						flower.getGrowingTips().setLighting(Flower.GrowingTips.Lighting.value(attribute.getValue()));
					} else if (name.equals("multiplying")) {
						event = reader.nextEvent();
						flower.setMultiplying(event.asCharacters().getData());
					}
				} else if (event.isEndElement()){
					EndElement endElement = event.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")) {
						flowers.add(flower);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flowers;
	}

	public void FileOutputXML(String outputXmlFile, Collection<Flower> items) throws FileNotFoundException,
			UnsupportedEncodingException, XMLStreamException {
		OutputStream os = new FileOutputStream(outputXmlFile);
		XMLStreamWriter xml = XMLOutputFactory.newInstance().createXMLStreamWriter(
				new OutputStreamWriter(os, "utf-8"));
		xml.writeStartDocument("UTF-8", "1.0");
		xml.writeCharacters("\n");
		xml.writeStartElement("flowers");
		//if (rootAt != null){
		if (attributes != null && namespaces != null){
			attributes.forEachRemaining(i -> {
				try {
					xml.writeAttribute((i.getName().getPrefix() != null) ?
							i.getName().getPrefix() + ":" + i.getName().getLocalPart()
							: i.getName().getLocalPart(), i.getValue());
				} catch (XMLStreamException e) {
					e.printStackTrace();
				}
			});
			namespaces.forEachRemaining(i -> {
				try {
					xml.writeNamespace(i.getPrefix(), i.getNamespaceURI());
				} catch (XMLStreamException e) {
					e.printStackTrace();
				}
			});
			//xml.writeAttribute();
			//xml.writeNamespace(null, namespace);
			//xml.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
		}
		//for (Map.Entry<String, String> i : rootAt.entrySet()) {
		//	xml.writeAttribute(i.getKey(), i.getValue());
		//}
		//}
		for (Flower i : items) {
			xml.writeCharacters("\n	");

			xml.writeStartElement("flower");
			xml.writeCharacters("\n		");

			xml.writeStartElement("name");
			xml.writeCharacters(i.getName());
			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeStartElement("soil");
			xml.writeCharacters(i.getSoil());
			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeStartElement("origin");
			xml.writeCharacters(i.getOrigin());
			xml.writeEndElement();
			xml.writeCharacters("\n		");


			xml.writeStartElement("visualParameters");
			xml.writeCharacters("\n			");

			xml.writeStartElement("stemColour");
			xml.writeCharacters(i.getVisualParameters().getStemColour());
			xml.writeEndElement();
			xml.writeCharacters("\n			");

			xml.writeStartElement("leafColour");
			xml.writeCharacters(i.getVisualParameters().getLeafColour());
			xml.writeEndElement();
			xml.writeCharacters("\n			");

			xml.writeStartElement("aveLenFlower");
			xml.writeAttribute("measure", i.getVisualParameters().getAveLenFlower().getMeasure());
			xml.writeCharacters(i.getVisualParameters().getAveLenFlower().getValue().toString());
			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeEndElement();
			xml.writeCharacters("\n		");


			xml.writeStartElement("growingTips");
			xml.writeCharacters("\n			");

			xml.writeStartElement("tempreture");
			xml.writeAttribute("measure", i.getGrowingTips().getTempreture().getMeasure());
			xml.writeCharacters(i.getGrowingTips().getTempreture().getValue().toString());
			xml.writeEndElement();
			xml.writeCharacters("\n			");

			xml.writeEmptyElement("lighting");
			xml.writeAttribute("lightRequiring", i.getGrowingTips().getLighting().get());
			//xml.writeEndElement();
			xml.writeCharacters("\n			");

			xml.writeStartElement("watering");
			xml.writeAttribute("measure", i.getGrowingTips().getWatering().getMeasure());
			xml.writeCharacters(i.getGrowingTips().getWatering().getValue().toString());
			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeStartElement("multiplying");
			xml.writeCharacters(i.getMultiplying());
			xml.writeEndElement();

			xml.writeCharacters("\n	");
			xml.writeEndElement();
		}
		xml.writeCharacters("\n");
		xml.writeEndElement();
		xml.writeEndDocument();
		xml.close();
	}
}