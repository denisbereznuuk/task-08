package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> list = domController.parse();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		list.sort(new NameComparator());
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.FileOutputXML(outputXmlFile, list);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		list = saxController.parse();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		list.sort(new SoilComparator());
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.FileOutputXML(outputXmlFile, list);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		list = staxController.parse();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		list.sort(new OriginComparator());
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.FileOutputXML(outputXmlFile, list);
	}

}
