package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.*;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private List<Flower> flowers = new LinkedList<>();
	private Flower current;
	private String thiss;
	private Attributes attr;
	private String namespace;
	private Map<String, String> rootAt = new LinkedHashMap<>();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public List<Flower> parse() throws SAXException, ParserConfigurationException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		SAXParser sp = factory.newSAXParser();
		sp.parse(new File(xmlFileName), this);
		return flowers;
	}

	//public List<Flower> getResult(){
	//	return flowers;
	//}

	@Override
	public void startDocument() throws SAXException {
		//System.out.println("Start parse XML...");
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		thiss = qName;
		attr = atts;
		if (qName.equals("flowers")){
			//System.out.println(namespaceURI);
			namespace = namespaceURI;
			//System.out.println(localName);
			//namespaces = atts;
			int len = atts.getLength();
			for (int i = 0; i < len; i++){
				rootAt.put(atts.getQName(i), atts.getValue(i));
			}
		}
		if (qName.equals("lighting")) {
			current.getGrowingTips().setLighting(Flower.GrowingTips.Lighting.value(
					attr.getValue("lightRequiring")));
		}
	}

	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		thiss = "";
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		//if (thiss.equals("flowers")){
		//	namespaces = attr;
		//}
		if (thiss.equals("flower")){
			if (current != null){
				flowers.add(current);
			}
			current = new Flower();
		}
		if (thiss.equals("name")) {
			current.setName(new String(ch, start, length));
		}
		if (thiss.equals("soil")) {
			current.setSoil(new String(ch, start, length));
		}
		if (thiss.equals("origin")) {
			current.setOrigin(new String(ch, start, length));
		}
		if (thiss.equals("visualParameters")) {
			current.setVisualParameters(new Flower.VisualParameters());
		}
		if (thiss.equals("stemColour")){
			current.getVisualParameters().setStemColour(new String(ch, start, length));
		}
		if (thiss.equals("leafColour")) {
			current.getVisualParameters().setLeafColour(new String(ch, start, length));
		}
		if (thiss.equals("aveLenFlower")) {
			current.getVisualParameters().setAveLenFlower(new Flower.VisualParameters.AveLenFlower());
			current.getVisualParameters().getAveLenFlower().setValue(Integer.parseInt(new String(ch, start, length)));
			current.getVisualParameters().getAveLenFlower().setMeasure(attr.getValue("measure"));
		}
		if (thiss.equals("growingTips")) {
			current.setGrowingTips(new Flower.GrowingTips());//new String(ch, start, length));
		}
		if (thiss.equals("tempreture")) {
			current.getGrowingTips().setTempreture(new Flower.GrowingTips.Tempreture());
			current.getGrowingTips().getTempreture().setValue(Integer.parseInt(new String(ch, start, length)));
			current.getGrowingTips().getTempreture().setMeasure(attr.getValue("measure"));
		}
		if (thiss.equals("watering")) {
			current.getGrowingTips().setWatering(new Flower.GrowingTips.Watering());
			current.getGrowingTips().getWatering().setValue(Integer.parseInt(new String(ch, start, length)));
			current.getGrowingTips().getWatering().setMeasure(attr.getValue("measure"));
		}
		if (thiss.equals("multiplying")) {
			current.setMultiplying(new String(ch, start, length));
		}
	}

	public void FileOutputXML(String outputXmlFile, Collection<Flower> items) throws FileNotFoundException,
			UnsupportedEncodingException, XMLStreamException {
		OutputStream os = new FileOutputStream(outputXmlFile);
		XMLStreamWriter xml = XMLOutputFactory.newInstance().createXMLStreamWriter(
				new OutputStreamWriter(os, "utf-8"));
		xml.writeStartDocument("UTF-8", "1.0");
		xml.writeCharacters("\n");
		xml.writeStartElement("flowers");
		//if (rootAt != null){
		if (namespace != null){
			xml.writeNamespace(null, namespace);
			xml.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
		}
		for (Map.Entry<String, String> i : rootAt.entrySet()) {
			xml.writeAttribute(i.getKey(), i.getValue());
		}
		//}
		for (Flower i : items) {
			xml.writeCharacters("\n	");

			xml.writeStartElement("flower");
			xml.writeCharacters("\n		");

			xml.writeStartElement("name");
			xml.writeCharacters(i.getName());
			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeStartElement("soil");
			xml.writeCharacters(i.getSoil());
			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeStartElement("origin");
			xml.writeCharacters(i.getOrigin());
			xml.writeEndElement();
			xml.writeCharacters("\n		");


			xml.writeStartElement("visualParameters");
			xml.writeCharacters("\n			");

			xml.writeStartElement("stemColour");
			xml.writeCharacters(i.getVisualParameters().getStemColour());
			xml.writeEndElement();
			xml.writeCharacters("\n			");

			xml.writeStartElement("leafColour");
			xml.writeCharacters(i.getVisualParameters().getLeafColour());
			xml.writeEndElement();
			xml.writeCharacters("\n			");

			xml.writeStartElement("aveLenFlower");
			xml.writeAttribute("measure", i.getVisualParameters().getAveLenFlower().getMeasure());
			xml.writeCharacters(i.getVisualParameters().getAveLenFlower().getValue().toString());
			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeEndElement();
			xml.writeCharacters("\n		");


			xml.writeStartElement("growingTips");
			xml.writeCharacters("\n			");

			xml.writeStartElement("tempreture");
			xml.writeAttribute("measure", i.getGrowingTips().getTempreture().getMeasure());
			xml.writeCharacters(i.getGrowingTips().getTempreture().getValue().toString());
			xml.writeEndElement();
			xml.writeCharacters("\n			");

			xml.writeEmptyElement("lighting");
			xml.writeAttribute("lightRequiring", i.getGrowingTips().getLighting().get());
			//xml.writeEndElement();
			xml.writeCharacters("\n			");

			xml.writeStartElement("watering");
			xml.writeAttribute("measure", i.getGrowingTips().getWatering().getMeasure());
			xml.writeCharacters(i.getGrowingTips().getWatering().getValue().toString());
			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeEndElement();
			xml.writeCharacters("\n		");

			xml.writeStartElement("multiplying");
			xml.writeCharacters(i.getMultiplying());
			xml.writeEndElement();

			xml.writeCharacters("\n	");
			xml.writeEndElement();
		}
		xml.writeCharacters("\n");
		xml.writeEndElement();
		xml.writeEndDocument();
		xml.close();
	}

	@Override
	public void endDocument() {
		//System.out.println("Stop parse XML...");
		if (current != null){
			flowers.add(current);
		}
	}
}

//class SAXHandler extends DefaultHandler	{
//	private List<Flower> flowers;
//	private StringBuilder elementValue;
//
//	@Override
//	public void characters(char[] ch, int start, int length) throws SAXException {
//		if (elementValue == null){
//			elementValue = new StringBuilder();
//		} else {
//			elementValue.append(ch, start, length);
//		}
//	}
//}