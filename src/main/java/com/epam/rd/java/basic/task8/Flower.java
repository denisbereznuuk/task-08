package com.epam.rd.java.basic.task8;

import java.util.Set;

public class Flower {

    private String name;
    private String soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private String multiplying;

    private static Set <String> SOILS = Set.of("подзолистая", "грунтовая", "дерново-подзолистая");
    private static Set <String> MULTYPLYINGS = Set.of("листья", "черенки", "семена");

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(String value) {
        if (!value.matches("([a-zA-Zа-яА-Я])+")){
            throw new IllegalArgumentException();
        }
        this.name = value;
    }

    /**
     * Gets the value of the soil property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSoil() {
        return soil;
    }

    /**
     * Sets the value of the soil property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSoil(String value) {
        if (!SOILS.contains(value)){
            throw new IllegalArgumentException();
        }
        this.soil = value;
    }

    /**
     * Gets the value of the origin property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOrigin(String value) {
        if (!value.matches("([a-zA-Zа-яА-Я])+")){
            throw new IllegalArgumentException();
        }
        this.origin = value;
    }

    /**
     * Gets the value of the visualParameters property.
     *
     * @return
     *     possible object is
     *     {@link VisualParameters }
     *
     */
    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    /**
     * Sets the value of the visualParameters property.
     *
     * @param value
     *     allowed object is
     *     {@link VisualParameters }
     *
     */
    public void setVisualParameters(VisualParameters value) {
        this.visualParameters = value;
    }

    /**
     * Gets the value of the growingTips property.
     *
     * @return
     *     possible object is
     *     {@link GrowingTips }
     *
     */
    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    /**
     * Sets the value of the growingTips property.
     *
     * @param value
     *     allowed object is
     *     {@link GrowingTips }
     *
     */
    public void setGrowingTips(GrowingTips value) {
        this.growingTips = value;
    }

    /**
     * Gets the value of the multiplying property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMultiplying() {
        return multiplying;
    }

    /**
     * Sets the value of the multiplying property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMultiplying(String value) {
        if (!MULTYPLYINGS.contains(value)){
            throw new IllegalArgumentException();
        }
        this.multiplying = value;
    }

    public static class GrowingTips {

        private Tempreture tempreture;
        private Lighting lighting;
        private Watering watering;

        /**
         * Gets the value of the tempreture property.
         *
         * @return
         *     possible object is
         *     {@link Tempreture }
         *
         */
        public Tempreture getTempreture() {
            return tempreture;
        }

        /**
         * Sets the value of the tempreture property.
         *
         * @param value
         *     allowed object is
         *     {@link Tempreture }
         *
         */
        public void setTempreture(Tempreture value) {
            this.tempreture = value;
        }

        /**
         * Gets the value of the lighting property.
         *
         * @return
         *     possible object is
         *     {@link Lighting }
         *
         */
        public Lighting getLighting() {
            return lighting;
        }

        /**
         * Sets the value of the lighting property.
         *
         * @param value
         *     allowed object is
         *     {@link Lighting }
         *
         */
        public void setLighting(Lighting value) {
            this.lighting = value;
        }

        /**
         * Gets the value of the watering property.
         *
         * @return
         *     possible object is
         *     {@link Watering }
         *
         */
        public Watering getWatering() {
            return watering;
        }

        /**
         * Sets the value of the watering property.
         *
         * @param value
         *     allowed object is
         *     {@link Watering }
         *
         */
        public void setWatering(Watering value) {
            this.watering = value;
        }

        public enum Lighting {
            YES("yes"), NO("no");
            private String value;
            Lighting (String value){
                this.value = value;
            }
            public String get(){
                return value;
            }
            public static Lighting value(String value){
                if (value.equals(YES.value)){
                    return YES;
                } else if (value.equals(NO.value)){
                    return NO;
                }
                throw new IllegalArgumentException();
            }
        }


        public static class Tempreture {
            private Integer value;
            private String measure;

            /**
             * Gets the value of the value property.
             *
             * @return
             *     possible object is
             *     {@link Integer }
             *
             */
            public Integer getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             *
             * @param value
             *     allowed object is
             *     {@link Integer }
             *
             */
            public void setValue(Integer value) {
                this.value = value;
            }

            /**
             * Gets the value of the measure property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getMeasure() {
                //if (measure == null) {
                //    return "celcius";
                //} else {
                //    return measure;
                //}
                return measure != null ? measure : "celcius";
            }

            /**
             * Sets the value of the measure property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setMeasure(String value) {
                this.measure = value;
            }

        }


        public static class Watering {

            private Integer value;
            private String measure;

            /**
             * Gets the value of the value property.
             *
             * @return
             *     possible object is
             *     {@link Integer }
             *
             */
            public Integer getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             *
             * @param value
             *     allowed object is
             *     {@link Integer }
             *
             */
            public void setValue(Integer value) {
                this.value = value;
            }

            /**
             * Gets the value of the measure property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getMeasure() {
                //if (measure == null) {
                //    return "mlPerWeek";
                //} else {
                //    return measure;
                //}
                return measure != null ? measure : "mlPerWeek";
            }

            /**
             * Sets the value of the measure property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setMeasure(String value) {
                this.measure = value;
            }

        }

    }


    public static class VisualParameters {

        private String stemColour;
        private String leafColour;
        private AveLenFlower aveLenFlower;

        /**
         * Gets the value of the stemColour property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getStemColour() {
            return stemColour;
        }

        /**
         * Sets the value of the stemColour property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setStemColour(String value) {
            if (!value.matches("([a-zA-Zа-яА-Я])+")){
                throw new IllegalArgumentException();
            }
            this.stemColour = value;
        }

        /**
         * Gets the value of the leafColour property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getLeafColour() {
            return leafColour;
        }

        /**
         * Sets the value of the leafColour property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setLeafColour(String value) {
            if (!value.matches("([a-zA-Zа-яА-Я])+")){
                throw new IllegalArgumentException();
            }
            this.leafColour = value;
        }

        /**
         * Gets the value of the aveLenFlower property.
         *
         * @return
         *     possible object is
         *     {@link AveLenFlower }
         *
         */
        public AveLenFlower getAveLenFlower() {
            return aveLenFlower;
        }

        /**
         * Sets the value of the aveLenFlower property.
         *
         * @param value
         *     allowed object is
         *     {@link AveLenFlower }
         *
         */
        public void setAveLenFlower(AveLenFlower value) {
            this.aveLenFlower = value;
        }

        public static class AveLenFlower {
            private Integer value;
            private String measure;

            /**
             * Gets the value of the value property.
             *
             * @return
             *     possible object is
             *     {@link Integer }
             *
             */
            public Integer getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             *
             * @param value
             *     allowed object is
             *     {@link Integer }
             *
             */
            public void setValue(Integer value) {
                this.value = value;
            }

            /**
             * Gets the value of the measure property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getMeasure() {
                //if (measure == null) {
                //    return "cm";
                //} else {
                //    return measure;
                //}
                return measure != null ? measure : "cm";
            }

            /**
             * Sets the value of the measure property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setMeasure(String value) {
                this.measure = value;
            }

        }

    }

}
